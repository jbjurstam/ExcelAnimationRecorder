﻿using NetOffice.ExcelApi.Tools;
using System.Linq;
using Office = NetOffice.OfficeApi;

namespace ExcelAnimationRecorder
{
    
    public partial class Addin : COMAddin
    {
        private static string ShapeDropDown_Selected = null;

        public int ShapeDropDown_getItemCount(Office.IRibbonControl control)
        {
            var sheet = _app.GetActiveSheet();

            for (int i = PropertyWatchers.Count - 1; i >- 0; i--)
            {
                var watcher = PropertyWatchers[i];
                if (!sheet.Shapes.Any(s => s.Name.Equals(watcher.ShapeName)))
                    PropertyWatchers.RemoveAt(i);
            }

            return sheet.Shapes.Count;
        }

        public string ShapeDropDown_getItemLabel(Office.IRibbonControl control, int index)
        {
            var sheet = _app.GetActiveSheet();
            return sheet.Shapes[index + 1].Name;
        }

        public bool ShapeDropDown_GetEnabled(Office.IRibbonControl control)
        {
            var sheet = _app.GetActiveSheet();
            return sheet.Shapes.Count > 0;
        }

        public void ShapeDropDown_onAction(Office.IRibbonControl control, string itemID, int itemIndex)
        {
            var sheet = _app.GetActiveSheet();
            var name = sheet.Shapes[itemIndex + 1].Name;

            ShapeDropDown_Selected = name;

            RefreshPropertiesComboBox();
            RefreshVariableNameEditBox();
        }

        private void RefreshShapesDropDown()
        {
            RibbonUI.InvalidateControl("ShapeDropDown");
        }
    }
}