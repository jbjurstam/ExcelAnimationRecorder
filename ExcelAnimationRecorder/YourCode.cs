﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Excel = NetOffice.ExcelApi;

namespace ExcelAnimationRecorder
{
    public partial class YourCode : UserControl, Excel.Tools.ITaskPane
    {
        private static Excel.Application _app = null;

        private const BindingFlags _getPropertyFlags = BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public;

        public YourCode()
        {           
            InitializeComponent();
        }

        public void WriteShapeReferences(List<PropertyWatcher> propertyWatchers)
        {
            foreach (var propertyWatcher in propertyWatchers.Where(p => p.Sheet == _app.GetActiveSheet()).ToList())
            {
                var shape = propertyWatcher.Sheet.Shapes.FirstOrDefault(s => s.Name == propertyWatcher.ShapeName);
                if (Equals(shape, null)) {
                    propertyWatchers.Remove(propertyWatcher);
                }
                else
                {
                    CodeBox.AppendText($"dim {propertyWatcher.VariableName} as Shape{Environment.NewLine}");
                    CodeBox.AppendText($"set {propertyWatcher.VariableName} = ThisWorkbook.Sheets(\"{propertyWatcher.Sheet.Name}\").Shapes(\"{propertyWatcher.ShapeName}\"){Environment.NewLine}");
                }
            }
        }

        public void WritePropertyWatchers(List<PropertyWatcher> propertyWatchers)
        {
            foreach (var propertyWatcher in propertyWatchers.Where(p => p.Sheet == _app.GetActiveSheet()).ToList())
            {
                var shape = propertyWatcher.Sheet.Shapes.FirstOrDefault(s => s.Name == propertyWatcher.ShapeName);
                if (Equals(shape, null))
                {
                    propertyWatchers.Remove(propertyWatcher);
                }
                else
                {
                    foreach (var propertyName in propertyWatcher.PropertyNames)
                    {
                        string propertyValueString = GetVal(propertyWatcher.ShapeName, propertyName);

                        CodeBox.AppendText($"{propertyWatcher.VariableName}.{propertyName} = {propertyValueString}{Environment.NewLine}");
                    }
                }
            }
        }

        private string GetVal(string shapeName, string propertyName)
        {
            var shape = _app.GetActiveSheet().Shapes[shapeName];

            var obj = ResolveObject(ref propertyName, shape);

            var type = obj.GetType();

            if (type == typeof(string))
            {
                return $"\"{obj.ToString()}\"";
            }

            if (type.IsEnum)
            {
                return obj.ToString();
            }

            if (type.IsPrimitive)
            {
                var dec = decimal.Parse(obj.ToString());
                return dec.ToString(CultureInfo.InvariantCulture);
            }

            return obj.ToString();
        }

        private static object ResolveObject(ref string propertyName, Excel.Shape shape)
        {
            try {

                object obj = shape;

                var chain = propertyName.Split('.');
                var newChain = new List<string>(chain.Length);
                var enumerator = chain.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    var indexedProp = new PropertySupport.PropAndIndexer((string)enumerator.Current);

                    var prop = obj.GetType().GetProperty(indexedProp.PropName, _getPropertyFlags);
                    newChain.Add(prop.Name + indexedProp.Indexer);
                    if (string.IsNullOrWhiteSpace(indexedProp.Indexer)) {
                        obj = prop.GetValue(obj, null);
                    }
                    else
                    {
                        object index;
                        int intIndex;
                        if (int.TryParse(indexedProp.Indexer, out intIndex))
                        {
                            index = intIndex;
                        }
                        else
                        {
                            index = indexedProp.Indexer;
                        }

                        obj = prop.GetValue(obj, null);
                        var indexer = obj.GetType().GetProperties().FirstOrDefault(p => p.GetIndexParameters().Any());
                        obj = indexer.GetValue(obj, new[] { index });
                    }
                }

                propertyName = string.Join(".", newChain);

                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception(nameof(ResolveObject), ex);
            }
        }

        public void OnConnection(Excel.Application application, NetOffice.OfficeApi._CustomTaskPane parentPane, object[] customArguments)
        {
            _app = application;
        }

        public void OnDisconnection()
        {
        }

        public void OnDockPositionChanged(NetOffice.OfficeApi.Enums.MsoCTPDockPosition position)
        {
            
        }

        public void OnVisibleStateChanged(bool visible)
        {
            
        }

    }
}
